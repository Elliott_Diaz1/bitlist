//
//  ToDoModel.swift
//  Bitlist
//
//  Created by Elliott Diaz on 6/28/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation


enum RepeatType: Int
{
    case Daily   = 0
    case Weekly  = 1
    case Monthly = 2
    case Yearly  = 3
}

struct ToDoModel
{
    var dueDate   : NSDate?
    var title     : String
    var favorited : Bool
    var completed : Bool
    
    var repeated  : RepeatType?
    var reminder  : NSDate?
}