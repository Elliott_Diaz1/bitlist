//
//  TodoTableViewCell.swift
//  Bitlist
//
//  Created by Elliott Diaz on 6/27/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import UIKit

class TodoTableViewCell: UITableViewCell
{
    // MARK: IBOutlets
    @IBOutlet weak var completeButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var titleLabel    : UILabel!
    @IBOutlet weak var dateLabel     : UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected( selected: Bool, animated: Bool )
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: IBActions
    @IBAction func completeButtonTapped( sender: UIButton )
    {
    }
    
    @IBAction func favoriteButtonTapped( sender: UIButton )
    {
    }
    
}
