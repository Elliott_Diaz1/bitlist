//
//  AddTodoTableViewCell.swift
//  Bitlist
//
//  Created by Elliott Diaz on 6/27/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import UIKit

class AddTodoTableViewCell: UITableViewCell
{
    @IBOutlet weak var addTodoTextField : UITextField!
    @IBOutlet weak var favoriteButton   : UIButton!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func favoriteButtonTapped(sender: UIButton)
    {
    }
}
