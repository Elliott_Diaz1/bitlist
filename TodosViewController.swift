//
//  TodosViewController.swift
//  Bitlist
//
//  Created by Elliott Diaz on 6/27/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import UIKit

class TodosViewController: UIViewController
{
    
    @IBOutlet weak var tableView: UITableView!
    
    var baseArray: [[ToDoModel]] = []

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate   = self

        let todo1 = ToDoModel( dueDate: NSDate(), title: "Study iOS" , favorited: false, completed: false, repeated: nil, reminder: nil )
        let todo2 = ToDoModel( dueDate: NSDate(), title: "Eat Dinner", favorited: false, completed: false, repeated: nil, reminder: nil )
        let todo3 = ToDoModel( dueDate: NSDate(), title: "Gym"       , favorited: false, completed: false, repeated: nil, reminder: nil )
        
        baseArray = [
                        [ todo1 , todo2 , todo3 ],
                        [                       ]
                    ]
        
        // print( baseArray )
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editBarButtonItemTapped( sender: UIBarButtonItem )
    {
        if sender.title == "Edit"
        {
            if tableView.editing
            {
                tableView.setEditing( false, animated: true )
                sender.title = "Edit"
            }
            else
            {
                tableView.setEditing( true , animated: true )
                sender.title = "Done"
            }
        }
    }
}

extension TodosViewController: UITableViewDataSource
{
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        if indexPath.section == 1
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath)
    {
        let currentToDo = baseArray[0][sourceIndexPath.row]
        
        baseArray[0].removeAtIndex( sourceIndexPath.row                                     )
        baseArray[0].insert       ( currentToDo         , atIndex: destinationIndexPath.row )
    }
    
    func numberOfSectionsInTableView( tableView: UITableView ) -> Int
    {
        return 3
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if editingStyle == UITableViewCellEditingStyle.Delete
        {
            tableView.beginUpdates()
            baseArray[indexPath.section - 1].removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths( [indexPath], withRowAnimation: UITableViewRowAnimation.Automatic )
            tableView.endUpdates()
        }
    }
    
    func tableView( tableView: UITableView, numberOfRowsInSection section: Int ) -> Int
    {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return baseArray[0].count
        }
        else if section == 2
        {
            return baseArray[1].count
        }
        else
        {
            return 0
        }
    }

    func tableView( tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath ) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let cell: AddTodoTableViewCell = tableView.dequeueReusableCellWithIdentifier( "AddToDoCell" ) as! AddTodoTableViewCell
            cell.backgroundColor           = UIColor.redColor()
            
            return cell
        }
        else if indexPath.section == 1 || indexPath.section == 2
        {
            let currentToDo                = baseArray[ indexPath.section - 1 ][ indexPath.row ]
            
            let cell: TodoTableViewCell    = tableView.dequeueReusableCellWithIdentifier( "Todocell" ) as! TodoTableViewCell
            cell.titleLabel.text           = currentToDo.title
            
            let dateStringFormatter        = NSDateFormatter()
            dateStringFormatter.dateFormat = "yyyy-MM-dd"
            
            if let date = currentToDo.dueDate
            {
                let dateString      = dateStringFormatter.stringFromDate( date )
                cell.dateLabel.text = dateString
            }
            
            if indexPath.section == 1
            {
                cell.completeButton.backgroundColor = UIColor.redColor()
            }
            else
            {
                cell.completeButton.backgroundColor = UIColor.greenColor()
            }
            
            if currentToDo.favorited
            {
                cell.favoriteButton.backgroundColor = UIColor.blueColor()
            }
            else
            {
                cell.favoriteButton.backgroundColor = UIColor.orangeColor()
            }
            
            cell.backgroundColor = UIColor( red: 235/255, green: 176/255, blue: 53/255, alpha: 0.7 )
            
            return cell
        }
        else
        {
            return UITableViewCell()
        }
    }
}

extension TodosViewController: UITableViewDelegate
{
    
}













































































































































